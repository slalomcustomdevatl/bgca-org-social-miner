﻿using BGCASocialLinkGenerator.Data;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BGCASocialLinkGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            BGCAEntities db = new BGCAEntities();
            var url = "http://bgca.cartodb.com/api/v2/sql?q=SELECT%20insight_id,name,url%20from%20bgca_organizations_2014_02_11%20WHERE%20(status%20IN%20(%27Active%20-%20Full%20Membership%27))%20AND%20url%20%3C%3E%20%27%27";
            var json = JsonConvert.DeserializeObject<BGCARep>(new WebClient().DownloadString(url));
            foreach (Item item in json.rows)
            {
                //create a object
                OrgMetaData neworg = new OrgMetaData()
                {
                    organizationid = item.insight_id,
                    name = item.name,
                    url = item.url
                };

                
                //load the doc, parse it, print the links to facebook
                Console.WriteLine(String.Format("Checking {0}", item.url));
                HtmlWeb web = new HtmlWeb();
                string cluburl = item.url;
                if (!cluburl.ToLower().StartsWith("http"))
                    cluburl = "http://" + cluburl;
                try {
                    db.OrgMetaDatas.Add(neworg);
                    db.SaveChanges();
                    HtmlDocument doc = web.Load(cluburl);
                    foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                    {
                        string value = link.Attributes["href"].Value.ToLower();
                        if (value.Contains("facebook.com") || value.Contains("twitter.com"))
                        {
                            if (value.Contains("facebook.com") && neworg.facebooklink == null)
                            {
                                neworg.facebooklink = value;
                                db.Entry(neworg).State = System.Data.Entity.EntityState.Modified;
                            }
                            else if (value.Contains("twitter.com") && neworg.twitterlink == null)
                            {
                                neworg.twitterlink = value;
                                db.Entry(neworg).State = System.Data.Entity.EntityState.Modified;
                            }
                            else if (neworg.otherlinks == null || !neworg.otherlinks.Contains(value))
                            {
                                if (neworg.otherlinks == null) neworg.otherlinks = value;
                                else neworg.otherlinks = neworg.otherlinks + "|" + value;
                                db.Entry(neworg).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                    }
                    db.SaveChanges();
                }
                catch { 
                    continue;
                }

            }
        }
    }
}
