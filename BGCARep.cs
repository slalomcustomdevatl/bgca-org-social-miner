﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGCASocialLinkGenerator
{
    class BGCARep
    {
        public List<Item> rows;
        public decimal time;
        public int total_rows;
    }

    class Item
    {
        public int insight_id;
        public string name;
        public string url;
    }
}
